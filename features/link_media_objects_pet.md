# Link media objects to pet

To link media objects to an animal, simply modify the animal and assign the IRIs representing the media objects to be linked.
Images can be linked during the creation of the animal or when the animal is modified.

So call 

`POST /api/v1/pets/{uuuid} `

or 

`PATCH /api/v1/pets/{uuuid}`

and fill in the `mainImage` and `images` fields as indicated in the documentation for these routes.

If you want to add secondary images to the old ones, you will also have to fill in the old IRIles in addition to the new ones.
# Login system

For the login system, nothing very complicated, all the user has to do is go to the login page and enter their credentials. The route:

`POST /api/v1/login_check`

is then called. This route will return a Bearer token, which will be used to authenticate the user throughout their experience within the application.

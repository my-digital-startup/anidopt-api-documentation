# Change password

To change the password, the user must be logged in. He will have at his disposal a form with 2 fields (password and confirmation of the new password).

Once the form is submitted, we call the

`PATCH /api/v1/users/{uuid}/password`

route which will return a response saying if the password is modified or not.

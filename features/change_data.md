# Change data

To change the data of the user, the user must be logged in. He will have at his disposal a form with some fields.
For adopter:

- First name
- Last name
- Pseudo
- Account informations

For shelter:

- Name
- Account informations

We call the

`PATCH /api/v1/adopters/{uuid}` OR `PATCH /api/v1/shelters/{uuid}`

route which will return a response saying if the data are modified or not.

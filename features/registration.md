# Registration

## First step

For the registration system, the user should go to the registration page of the application. They will then fill in the form and submit it.

If the person ticks the box "Are you a shelter?", then route:

`POST api/v1/shelters`

should be called, and the body will contains the name. Otherwise route:

`POST api/v1/adopters`

should be called, with only the email and password.

## Second step

Once the user is registered, it is entered into the database. They are sent an email containing a link to activate their account. They must then click on this link.

If the activation process is successful, the user will be redirected to an error page predefined in an environment variable. If it goes through correctly, the principle will be the same, he will be sent back to the login page.

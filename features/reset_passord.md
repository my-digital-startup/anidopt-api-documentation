# Forgotten password

## First step

The logged out user clicks on the "Forgot your password?" link. They are taken to a page with an email field. The person fills in their email, and once submitted, the

`PUT /api/v1/users/password/reset`

route is called and a JSON body containing only the email. This returns a response as the email is sent (or not).

## Second step

Once the email is sent, the user must click on the link contained in the email, which links to a selected page of the front-end application. This link contains the token and the user's own email.

Once on this page, the front-end developer retrieves the token and email as soon as the page loads.

The user will then see a form to enter their new password and confirm it by rewriting it. Once the form is submitted, the

`PUT /api/v1/users/password/reset`

route is called and a JSON body containing the email, new password and token is added.

The response will be a confirmation of a modified (or not) password.

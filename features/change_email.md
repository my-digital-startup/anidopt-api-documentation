# Change email

To change the email, the user must be logged in. He will have at his disposal a form with 1 field (email).

Once the form is submitted, we ask him is password to confirm it's him. We call the

`PATCH /api/v1/users/{uuid}/email`

route which will return a response saying if the email is modified or not.

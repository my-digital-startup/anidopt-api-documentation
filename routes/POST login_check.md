# API Route

## Name

`POST login_check`

## Description

This route is used to retrieve a JWT token (Bearer) in order to authenticate within the API.

## Request

```
POST /api/v1/login_check
```

## Query parameters

`None`

## Body JSON

```JSON
{
  "username": "adopter@anidopt.bzh",
  "password": "password"
}
```

## Response

```JSON
{
  "token": "XXXXXX"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

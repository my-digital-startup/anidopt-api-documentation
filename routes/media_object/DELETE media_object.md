# API Route

## Name

`DELETE media_object`

## Description

This route delete a specific media object.

## Request

```
DELETE /api/v1/media_objects/{id}
```

## Query parameters

`id: String`

## Body JSON

`None`

## Response

It's a 204 No Content Response.

## Authorization

`IS_AUTHENTICATED_FULLY` <br/>
`ROLE_SHELTER` (Only the shelter that created this image)

# API Route

## Name

`GET media_objects`

## Description

This route retrieves all media objects.

## Request

```
GET /api/v1/media_objects
```

## Query parameters

`None`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/MediaObject",
  "@id": "\/api\/v1\/media_objects",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/v1\/media_objects\/1",
      "@type": "MediaObject",
      "contentUrl": "\/media\/60e8394e9aedb_bob.jpeg",
      "pet": "\/api\/v1\/pets\/37429c3d-91ba-42ce-b992-66bfde1f7d23",
      "author": "\/api\/v1\/shelters\/d0ef8d84-1323-478e-90d7-c22b9e95600c"
    },
    {
      // An other Media Object
    },
  ],
  "hydra:totalItems": 4
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`POST media_object`

## Description

This route allows you to create a media object.

## Request

```
POST /api/v1/media_objects
```

## Query parameters

`None`

## Body Multipart

- file : `FILE`

If you are not familiar with Multipart, here is [an example](https://stackoverflow.com/questions/41878838/how-do-i-set-multipart-in-axios-with-react#answer-42096508) of how to integrate it in Javascript using Axios

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/MediaObject",
  "@id": "\/api\/v1\/media_objects\/5",
  "@type": "MediaObject",
  "contentUrl": "\/media\/60e83b22acfdf_top-5.png",
  "pet": null,
  "author": "\/api\/v1\/shelters\/d0ef8d84-1323-478e-90d7-c22b9e95600c"
}
```

## Authorization

`IS_AUTHENTICATED_FULLY` <br/>
`ROLE_SHELTER`

# API Route

## Name

`GET media_object`

## Description

This route retrieves a specific media object.

## Request

```
GET /api/v1/media_objects/{id}
```

## Query parameters

`id: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/MediaObject",
  "@id": "\/api\/v1\/media_objects\/1",
  "@type": "MediaObject",
  "contentUrl": "\/media\/60e8394e9aedb_bob.jpeg",
  "pet": "\/api\/v1\/pets\/37429c3d-91ba-42ce-b992-66bfde1f7d23",
  "author": "\/api\/v1\/shelters\/d0ef8d84-1323-478e-90d7-c22b9e95600c"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`
# API Route

## Name

`DELETE breed`

## Description

This route delete a specific pet.

## Request

```
DELETE /api/v1/pets/{uuid}
```

## Query parameters

`uuid: String`

## Body JSON

`None`

## Response

It's a 204 No Content Response.

## Authorization

`IS_AUTHENTICATED_FULLY` <br/>
`ROLE_SHELTER` (Only the shelter that created this pet)

# API Route

## Name

`POST pet`

## Description

This route allows to create a pet.

## Request

```
POST /api/v1/pets/
```

## Query parameters

`None`

## Body JSON

```JSON
{
	"name": "POST PET name",
	"description": "POST PET description",
	"isMale": true,
	"isSterilised": false,
	"birthday": "2020-07-06T13:51:57+00:00",
	"identificationNumber": "POST PET identification number",
	"specie": "/api/v1/species/1",
	"shelter": "/api/v1/shelters/1f82a78e-16a2-40f0-80f9-3a9464a4e9d7",
	"breeds": [
		"/api/v1/breeds/1"
	],
	"tags": [
		"/api/v1/tags/1",
		"/api/v1/tags/2",
		"/api/v1/tags/3"
	],
	"mainImage": "/api/v1/media_objects/1",
	"images": [
		"/api/v1/media_objects/2",
		"/api/v1/media_objects/3"
	]
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Pet",
  "@id": "\/api\/v1\/pets\/ec27dd88-faf5-474d-8853-dcb33a2c7580",
  "@type": "Pet",
  "id": 76,
  "uuid": "ec27dd88-faf5-474d-8853-dcb33a2c7580",
  "name": "POST PET name",
  "slug": "post-pet-name-ec27dd88-faf5-474d-8853-dcb33a2c7580",
  "description": "POST PET description",
  "isMale": true,
  "birthday": "2020-07-06T13:51:57+00:00",
  "isSterilised": false,
  "identificationNumber": "POST PET identification number",
  "shelter": {
    "@id": "\/api\/v1\/shelters\/1f82a78e-16a2-40f0-80f9-3a9464a4e9d7",
    "@type": "Shelter",
    "name": "Jeanne Peltier",
    "slug": "jeanne-peltier",
    "account": {
      "@type": "Account",
      "@id": "_:906",
      "address": "rue de Robin",
      "city": "Petitjean-sur-Mer",
      "zipCode": "62 827"
    }
  },
  "specie": {
    "@id": "\/api\/v1\/species\/1",
    "@type": "Specie",
    "id": 1,
    "name": "Chat"
  },
  "breeds": [
    {
      "@id": "\/api\/v1\/breeds\/1",
      "@type": "Breed",
      "id": 1,
      "name": "Dog breed 0similique"
    }
  ],
  "tags": [
    {
      "@id": "\/api\/v1\/tags\/1",
      "@type": "Tag",
      "id": 1,
      "name": "Tag 0sed"
    },
    {
      "@id": "\/api\/v1\/tags\/2",
      "@type": "Tag",
      "id": 2,
      "name": "Tag 1ut"
    },
    {
      "@id": "\/api\/v1\/tags\/3",
      "@type": "Tag",
      "id": 3,
      "name": "Tag 2quo"
    }
  ],
  "mainImage": {
    "@id": "\/api\/v1\/media_objects\/1",
    "@type": "MediaObject",
    "contentUrl": "\/media\/60e83f816f6b3_bob.jpeg"
  },
  "images": [
    {
      "@id": "\/api\/v1\/media_objects\/2",
      "@type": "MediaObject",
      "contentUrl": "\/media\/60e83f8170efa_emilien.jpeg"
    },
    {
      "@id": "\/api\/v1\/media_objects\/3",
      "@type": "MediaObject",
      "contentUrl": "\/media\/60e83f8171ea7_julien.jpeg"
    }
  ],
  "createdAt": "2021-07-09T12:23:32+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` <br/>
`ROLE_SHELTER`
# API Route

## Name

`PATCH pet`

## Description

This route allows a pet's information to be changed.

## Request

```
PATCH /api/v1/pets/{uuid}
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`uuid: String`

## Body JSON

```JSON
{
	"name": "PATH PET name",
	"description": "PATCH PET description",
	"isMale": true,
	"isSterilised": false,
	"birthday": "2020-07-06T13:51:57+00:00",
	"identificationNumber": "PATCH PET identification number",
  "isSos": false,
	"specie": "/api/v1/species/1",
	"breeds": [
		"/api/v1/breeds/1"
	],
	"tags": [
		"/api/v1/tags/1",
		"/api/v1/tags/2",
		"/api/v1/tags/3"
	],
	"mainImage": "/api/v1/media_objects/1",
	"images": [
		"/api/v1/media_objects/2",
		"/api/v1/media_objects/3"
	]
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Pet",
  "@id": "\/api\/v1\/pets\/58895318-c054-4f35-a146-ae84144360dd",
  "@type": "Pet",
  "id": 1,
  "uuid": "58895318-c054-4f35-a146-ae84144360dd",
  "name": "PATH PET name",
  "slug": "matthieu-maurice-raymond-58895318-c054-4f35-a146-ae84144360dd",
  "description": "PATCH PET description",
  "isMale": true,
  "birthday": "2020-07-06T13:51:57+00:00",
  "isSterilised": false,
  "identificationNumber": "PATCH PET identification number",
  "isSos": false,
  "shelter": {
    "@id": "\/api\/v1\/shelters\/7bff18ec-9b0c-4e8b-8a37-954767eaea67",
    "@type": "Shelter",
    "name": "Grégoire Morvan",
    "slug": "gregoire-morvan",
    "account": {
      "@type": "Account",
      "@id": "_:906",
      "address": "87, impasse Lefebvre",
      "city": "Rogerboeuf",
      "zipCode": "06505"
    }
  },
  "specie": {
    "@id": "\/api\/v1\/species\/1",
    "@type": "Specie",
    "id": 1,
    "name": "Chat"
  },
  "breeds": [
    {
      "@id": "\/api\/v1\/breeds\/1",
      "@type": "Breed",
      "id": 1,
      "name": "Dog breed 0dolor"
    }
  ],
  "tags": [
    {
      "@id": "\/api\/v1\/tags\/1",
      "@type": "Tag",
      "id": 1,
      "name": "Tag 0autem"
    },
    {
      "@id": "\/api\/v1\/tags\/2",
      "@type": "Tag",
      "id": 2,
      "name": "Tag 1dicta"
    },
    {
      "@id": "\/api\/v1\/tags\/3",
      "@type": "Tag",
      "id": 3,
      "name": "Tag 2cumque"
    }
  ],
  "mainImage": {
    "@id": "\/api\/v1\/media_objects\/1",
    "@type": "MediaObject",
    "contentUrl": "\/media\/60e83ef515d3c_bob.jpeg"
  },
  "images": [
    {
      "@id": "\/api\/v1\/media_objects\/2",
      "@type": "MediaObject",
      "contentUrl": "\/media\/60e83ef516c91_emilien.jpeg"
    },
    {
      "@id": "\/api\/v1\/media_objects\/3",
      "@type": "MediaObject",
      "contentUrl": "\/media\/60e83ef51773e_julien.jpeg"
    }
  ],
  "createdAt": "2021-07-09T12:20:05+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` <br/>
`ROLE_SHELTER` (Only the shelter that created this pet)

# API Route

## Name

`GET pets`

## Description

This route retrieves all pets.

## Request

```
GET /api/v1/pets
```

## Query parameters

### Date Filter

- `birthday`

Example : /api/v1/pets?`birthday`[after|before]=2021-12-15

### Boolean Filter

- `isMale`
- `isSterilised`
- `isSos`

Example : /api/v1/pets?`isMale`=true&`isSterilised`=false&`isSos`=true

### Search Filter

- `specie.name` => exact
- `breeds.name` => exact
- `tag.name` => exact

Example : /api/v1/pets?`specie.name`=Specie&`breeds.name`=Breed&`tag.name`=Tag

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Pet",
  "@id": "\/api\/v1\/pets",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/v1\/pets\/25a3aa5d-2e6a-4463-bce6-d1ac573b7c9f",
      "@type": "Pet",
      "id": 1,
      "uuid": "25a3aa5d-2e6a-4463-bce6-d1ac573b7c9f",
      "name": "Alex-Suzanne Delannoy",
      "slug": "alex-suzanne-delannoy-25a3aa5d-2e6a-4463-bce6-d1ac573b7c9f",
      "isMale": false,
      "identificationNumber": "fbd45b5f-b1ac-4fa3-ae30-3fef0f7920a4",
      "isSos": false,
      "birthday": "2021-07-05T12:23:24+00:00",
      "shelter": {
        "@id": "\/api\/v1\/shelters\/d75fed70-3efd-4d14-8aac-8109333e21ba",
        "@type": "Shelter",
        "name": "Victor Lombard",
        "slug": "victor-lombard",
        "account": {
          "@type": "Account",
          "@id": "_:5249",
          "city": "Renaud"
        }
      },
      "specie": {
        "@id": "\/api\/v1\/species\/2",
        "@type": "Specie",
        "id": 2,
        "name": "Chien"
      },
      "breeds": [
        {
          "@id": "\/api\/v1\/breeds\/25",
          "@type": "Breed",
          "id": 25,
          "name": "Dog breed 8iusto"
        }
      ],
      "createdAt": "2021-07-05T12:23:24+00:00"
    },
    {
      // An other pet
    },
  ],
  "hydra:totalItems": 69,
  "hydra:view": {
    "@id": "\/api\/v1\/pets?page=1",
    "@type": "hydra:PartialCollectionView",
    "hydra:first": "\/api\/v1\/pets?page=1",
    "hydra:last": "\/api\/v1\/pets?page=3",
    "hydra:next": "\/api\/v1\/pets?page=2"
  }
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

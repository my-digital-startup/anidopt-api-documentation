# API Route

## Name

`GET pet`

## Description

This route retrieves a specific pet.

## Request

```
GET /api/v1/pets/{uuid}
```

## Query parameters

`uuid: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Pet",
  "@id": "\/api\/v1\/pets\/25a3aa5d-2e6a-4463-bce6-d1ac573b7c9f",
  "@type": "Pet",
  "id": 1,
  "uuid": "25a3aa5d-2e6a-4463-bce6-d1ac573b7c9f",
  "name": "Alex-Suzanne Delannoy",
  "slug": "alex-suzanne-delannoy-25a3aa5d-2e6a-4463-bce6-d1ac573b7c9f",
  "description": "Deserunt voluptatibus qui laboriosam perspiciatis at dolorem. Repellat provident officia ut non excepturi molestias molestiae. Eveniet qui laboriosam deserunt iure earum commodi.",
  "isMale": false,
  "birthday": "2021-07-05T12:23:24+00:00",
  "isSterilised": false,
  "identificationNumber": "fbd45b5f-b1ac-4fa3-ae30-3fef0f7920a4",
  "isSos": false,
  "shelter": {
    "@id": "\/api\/v1\/shelters\/d75fed70-3efd-4d14-8aac-8109333e21ba",
    "@type": "Shelter",
    "name": "Victor Lombard",
    "slug": "victor-lombard",
    "account": {
      "@type": "Account",
      "@id": "_:1305",
      "phone": null,
      "address": "place Jeanne Hubert",
      "city": "Renaud",
      "zipCode": "61 120"
    }
  },
  "specie": {
    "@id": "\/api\/v1\/species\/2",
    "@type": "Specie",
    "id": 2,
    "name": "Chien"
  },
  "breeds": [
    {
      "@id": "\/api\/v1\/breeds\/25",
      "@type": "Breed",
      "id": 25,
      "name": "Dog breed 8iusto"
    }
  ],
  "tags": [
    {
      "@id": "\/api\/v1\/tags\/3",
      "@type": "Tag",
      "id": 3,
      "name": "Tag eligendi"
    },
    {
      "@id": "\/api\/v1\/tags\/5",
      "@type": "Tag",
      "id": 5,
      "name": "Tag magnam"
    },
    {
      "@id": "\/api\/v1\/tags\/6",
      "@type": "Tag",
      "id": 6,
      "name": "Tag qui"
    },
    {
      "@id": "\/api\/v1\/tags\/8",
      "@type": "Tag",
      "id": 8,
      "name": "Tag sit"
    }
  ],
  "createdAt": "2021-07-05T12:23:24+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`GET adopters`

## Description

This route retrieves all adopters (users).

## Request

```
GET /api/v1/adopters
```

## Query parameters

`None`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "/api/v1/contexts/Adopter",
  "@id": "/api/v1/adopters",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "/api/v1/adopters/dbd02dac-244a-4315-89ab-d9eb529affc3",
      "@type": "Adopter",
      "id": null,
      "uuid": "dbd02dac-244a-4315-89ab-d9eb529affc3",
      "firstName": "Bernard",
      "lastName": "",
      "pseudo": "Martine Garcia",
      "email": "adopter@anidopt.bzh",
      "roles": [
        "ROLE_USER",
        "ROLE_ADOPTER"
      ],
      "createdAt": "2021-06-09T10:37:41+00:00",
      "account": {
        "@type": "Account",
        "@id": "_:2404",
        "activationToken": "400ef945cf57813c5a3e57e4b013d6d7",
        "isVerified": false,
        "resetPasswordToken": null
      }
    },
    {
      // An other adopter
    },
  ],
  "hydra:totalItems": 19
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`PATCH favorite_adopters`

## Description

This route allows an adopter to change is liked pets.

## Request

```
PATCH /api/v1/adopters/{uuid}/pets
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`uuid: String`

## Body JSON

```JSON
{
	"petUuid": "41bc3fbd-9541-497f-8711-70c27bc16796"
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Adopter",
  "@id": "\/api\/v1\/adopters\/b25f336c-a786-4dfa-8efd-32bac158317b",
  "@type": "Adopter",
  "uuid": "b25f336c-a786-4dfa-8efd-32bac158317b",
  "firstName": "",
  "lastName": "Roussel",
  "pseudo": "",
  "favoritePets": [
    "\/api\/v1\/pets\/1e99d8cc-3ad2-4ef5-b522-b473ec6e32b6",
    // Another pet
  ],
  "email": "adopter@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_ADOPTER"
  ],
  "createdAt": "2021-09-14T12:10:58+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:1058",
    "isVerified": true,
    "address": "79, boulevard de Menard",
    "city": "Delahayenec",
    "zipCode": "67 076"
  }
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`GET favorite_pets`

## Description

This route retrieves all favorite pets of the user.

## Request

```
GET /api/v1/adopters/{uuid}/pets
```

## Query parameters

`uuid: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Pet",
  "@id": "\/api\/v1\/adopters\/b25f336c-a786-4dfa-8efd-32bac158317b\/pets",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/v1\/pets\/41bc3fbd-9541-497f-8711-70c27bc16796",
      "@type": "Pet",
      "id": 2,
      "uuid": "41bc3fbd-9541-497f-8711-70c27bc16796",
      "name": "Laurence Lemoine",
      "slug": "laurence-lemoine-41bc3fbd-9541-497f-8711-70c27bc16796",
      "description": "Culpa possimus hic et qui tenetur cupiditate quia esse. Sint omnis vero assumenda voluptas. Qui alias ad nisi blanditiis laboriosam necessitatibus quo. Aut rerum qui praesentium error doloremque.",
      "isMale": true,
      "birthday": "2021-09-14T12:10:58+00:00",
      "isSterilised": true,
      "identificationNumber": "21072358-74e5-4be3-bcad-4eeaaead5047",
      "shelter": "\/api\/v1\/shelters\/3e280e7a-1391-4289-b3a7-a0998c39f119",
      "specie": "\/api\/v1\/species\/3",
      "breeds": [
        "\/api\/v1\/breeds\/14"
      ],
      "tags": [
        "\/api\/v1\/tags\/7"
      ],
      "mainImage": "\/api\/v1\/media_objects\/1",
      "images": [],
      "createdAt": "2021-09-14T12:10:58+00:00"
    },
    {
      // An other pet
    },
  ],
  "hydra:totalItems": 42,
  "hydra:view": {
    "@id": "\/api\/v1\/adopters\/b25f336c-a786-4dfa-8efd-32bac158317b\/pets?page=1",
    "@type": "hydra:PartialCollectionView",
    "hydra:first": "\/api\/v1\/adopters\/b25f336c-a786-4dfa-8efd-32bac158317b\/pets?page=1",
    "hydra:last": "\/api\/v1\/adopters\/b25f336c-a786-4dfa-8efd-32bac158317b\/pets?page=2",
    "hydra:next": "\/api\/v1\/adopters\/b25f336c-a786-4dfa-8efd-32bac158317b\/pets?page=2"
  }
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

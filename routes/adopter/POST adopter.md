# API Route

## Name

`POST adopter`

## Description

This route allows you to create an adopter.

## Request

```
POST /api/v1/adopters
```

## Query parameters

`None`

## Body JSON

```JSON
{
  "email": "adopter+200@anidopt.bzh",
  "password": "password"
}
```

## Response

```JSON
{
  "@context": "/api/v1/contexts/Adopter",
  "@id": "/api/v1/adopters/102ac42a-77ed-4b54-a4e0-6767eff148de",
  "@type": "Adopter",
  "id": null,
  "uuid": "102ac42a-77ed-4b54-a4e0-6767eff148de",
  "firstName": null,
  "lastName": null,
  "pseudo": null,
  "email": "adopter+200@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_ADOPTER"
  ],
  "createdAt": "2021-06-10T11:59:32+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:1051",
    "activationToken": "b2a8161ebcdba2dab45f309519b1fc23",
    "isVerified": false,
    "resetPasswordToken": null
  }
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

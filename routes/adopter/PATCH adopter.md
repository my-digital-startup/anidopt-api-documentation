# API Route

## Name

`PATCH adopter`

## Description

This route allows an adopter's information to be changed.

## Request

```
PATCH /api/v1/adopters/{uuid}
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`uuid: String`

## Body JSON

```JSON
{
  "firstName": "FirstName",
  "lastName": "LastName",
  "pseudo": "Pseudo",
  "account": {
    "adresse": "Address",
    "city": "City",
    "zipCode": "123456"
  }
}
```

## Response

```JSON
{
  "@context": "/api/v1/contexts/Adopter",
  "@id": "/api/v1/adopters/2523b6a8-0bdd-4bd8-a6b4-c0192a555104",
  "@type": "Adopter",
  "uuid": "2523b6a8-0bdd-4bd8-a6b4-c0192a555104",
  "firstName": "FirstName",
  "lastName": "LastName",
  "pseudo": "Pseudo",
  "email": "adopter@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_ADOPTER"
  ],
  "createdAt": "2021-06-12T16:51:24+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:822",
    "activationToken": "b916e8c61ce0e7cff788b54f4ed9d4ff",
    "isVerified": false,
    "address": "9, avenue Audrey Blot",
    "city": "City",
    "zipCode": "123456"
  }
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` (Only the current user)

# API Route

## Name

`GET shelters`

## Description

This route retrieves all shelters (users).

## Request

```
GET /api/v1/shelters
```

## Query parameters

### Search Filter

- `name` => partial
- `account.zipCode` => exact
- `account.city` => partial

Example : /api/v1/pets?`name`=name&`account.zipCode`=Zip&`account.city`=City

## Body JSON

`None`

## Response

```JSON
{
  "@context": "/api/v1/contexts/Adopter",
  "@id": "/api/v1/adopters",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "/api/v1/shelters/2cab156b-32a9-4220-8f5e-a6818ba32073",
      "@type": "Shelter",
      "id": null,
      "uuid": "2cab156b-32a9-4220-8f5e-a6818ba32073",
      "name": "Renée Pottier",
      "slug": "renee-pottier",
      "email": "shelter@anidopt.bzh",
      "roles": [
        "ROLE_USER",
        "ROLE_SHELTER"
      ],
      "createdAt": "2021-06-09T10:37:45+00:00",
      "account": {
        "@type": "Account",
        "@id": "_:2298",
        "activationToken": "36612b0b437719898b6291e6bf1d1e20",
        "isVerified": false,
        "resetPasswordToken": null
      }
    },
    {
      // An other shelter
    },
    // Etc...
  ],
  "hydra:totalItems": 6
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

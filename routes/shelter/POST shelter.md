# API Route

## Name

`POST shelter`

## Description

This route allows you to create a shelter.

## Request

```
POST /api/v1/shelters
```

## Query parameters

`None`

## Body JSON

```JSON
{
  "email": "shelter+200@anidopt.com",
  "password": "password",
  "name": "Anidopt Gaming"
}
```

## Response

```JSON
{
  "@context": "/api/v1/contexts/Shelter",
  "@id": "/api/v1/shelters/7ecc2bc7-a634-4684-85f0-603b33e4d31d",
  "@type": "Shelter",
  "id": null,
  "uuid": "7ecc2bc7-a634-4684-85f0-603b33e4d31d",
  "name": "Anidopt Gaming",
  "slug": "anidopt-gaming",
  "email": "shelter+200@anidopt.com",
  "roles": [
    "ROLE_USER",
    "ROLE_SHELTER"
  ],
  "createdAt": "2021-06-10T13:07:19+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:7608",
    "activationToken": "e9e982b1b69a2e6d2b402339e9ff667c",
    "isVerified": false,
    "resetPasswordToken": null
  }
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

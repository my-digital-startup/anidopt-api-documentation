# API Route

## Name

`GET shelter`

## Description

This route retrieves a specific shelter (user).

## Request

```
GET /api/v1/shelters/{uuid}
```

## Query parameters

`uuid: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Shelter",
  "@id": "\/api\/v1\/shelters\/1f82a78e-16a2-40f0-80f9-3a9464a4e9d7",
  "@type": "Shelter",
  "uuid": "1f82a78e-16a2-40f0-80f9-3a9464a4e9d7",
  "name": "Jeanne Peltier",
  "slug": "jeanne-peltier",
  "pets": [
    {
      "@id": "\/api\/v1\/pets\/fc30daa2-105d-4712-85df-4e5ae949d3d8",
      "@type": "Pet",
      "id": 2,
      "uuid": "fc30daa2-105d-4712-85df-4e5ae949d3d8",
      "name": "Noël Benard",
      "slug": "noel-benard-fc30daa2-105d-4712-85df-4e5ae949d3d8",
      "isMale": true,
      "birthday": "2021-07-09T12:22:25+00:00",
      "identificationNumber": "94f5d334-3563-4ee4-908a-27ea96a5f91f",
      "specie": {
        "@id": "\/api\/v1\/species\/1",
        "@type": "Specie",
        "id": 1,
        "name": "Chat"
      },
      "breeds": [
        {
          "@id": "\/api\/v1\/breeds\/8",
          "@type": "Breed",
          "id": 8,
          "name": "Cat breed 2quidem"
        }
      ],
      "createdAt": "2021-07-09T12:22:25+00:00"
    },
    {
      // An other Pet
    }
  ],
  "email": "shelter@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_SHELTER"
  ],
  "createdAt": "2021-07-09T12:22:24+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:1364",
    "isVerified": true,
    "address": "rue de Robin",
    "city": "Petitjean-sur-Mer",
    "zipCode": "62 827"
  }
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` (Only the current user) <br/>
`ROLE_USER`

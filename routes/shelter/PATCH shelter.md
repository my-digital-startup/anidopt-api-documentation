# API Route

## Name

`PATCH shelter`

## Description

This route allows a shelter's information to be changed.

## Request

```
PATCH /api/v1/shelters/{uuid}
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`uuid: String`

## Body JSON

```JSON
{
  "name": "Name",
  "account": {
    "adresse": "Address",
    "city": "City",
    "zipCode": "123456"
  }
}
```

## Response

```JSON
{
  "@context": "/api/v1/contexts/Shelter",
  "@id": "/api/v1/shelters/ba40dc04-1e37-4281-8e5c-6257d48520dd",
  "@type": "Shelter",
  "uuid": "ba40dc04-1e37-4281-8e5c-6257d48520dd",
  "name": "Name",
  "slug": "stephane-fernandez",
  "email": "shelter@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_SHELTER"
  ],
  "createdAt": "2021-06-12T16:51:27+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:822",
    "activationToken": "d395a0805ea3cffc2aa2bbc6139add2a",
    "isVerified": false,
    "address": "65, boulevard de Samson",
    "city": "City",
    "zipCode": "123456"
  }
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` (Only the current user)

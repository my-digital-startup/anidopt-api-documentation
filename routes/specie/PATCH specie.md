# API Route

## Name

`PATCH specie`

## Description

This route allows to modify a specie.

## Request

```
PATCH /api/v1/species/{id}
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`id: String`

## Body JSON

```JSON
{
  "name": "PATCH SPECIE name",
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Specie",
  "@id": "\/api\/v1\/species\/1",
  "@type": "Specie",
  "id": 1,
  "name": "PATCH SPECIE name",
  "createdAt": "2021-07-09T12:22:25+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

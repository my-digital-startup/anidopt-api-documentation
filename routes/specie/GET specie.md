# API Route

## Name

`GET specie`

## Description

This route retrieves a specific specie.

## Request

```
GET /api/v1/species/{id}
```

## Query parameters

`id: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Specie",
  "@id": "\/api\/v1\/species\/1",
  "@type": "Specie",
  "id": 1,
  "name": "Chat",
  "createdAt": "2021-07-05T12:23:24+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

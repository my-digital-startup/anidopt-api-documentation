# API Route

## Name

`POST specie`

## Description

This route allows you to create a specie.

## Request

```
POST /api/v1/species
```

## Query parameters

`None`

## Body JSON

```JSON
{
  "name": "POST SPECIE name"
}

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Specie",
  "@id": "\/api\/v1\/species\/1",
  "@type": "Specie",
  "id": 1,
  "name": "PATCH SPECIE name",
  "createdAt": "2021-07-09T12:22:25+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

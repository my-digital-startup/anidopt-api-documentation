# API Route

## Name

`GET species`

## Description

This route retrieves all species.

## Request

```
GET /api/v1/species
```

## Query parameters

`None`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Specie",
  "@id": "\/api\/v1\/species",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/v1\/species\/1",
      "@type": "Specie",
      "id": 1,
      "name": "Chat",
      "createdAt": "2021-07-05T12:23:24+00:00"
    },
    {
      // An other specie
    }
  ],
  "hydra:totalItems": 3
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

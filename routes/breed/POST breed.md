# API Route

## Name

`POST breed`

## Description

This route allows you to create a breed.

## Request

```
POST /api/v1/breeds
```

## Query parameters

`None`

## Body JSON

```JSON
{
  "name": "POST BREED name"
}

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Breed",
  "@id": "\/api\/v1\/breeds\/31",
  "@type": "Breed",
  "id": 31,
  "name": "POST BREED name",
  "createdAt": "2021-07-09T12:10:58+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`DELETE breed`

## Description

This route delete a specific breed.

## Request

```
DELETE /api/v1/breeds/{id}
```

## Query parameters

`id: String`

## Body JSON

`None`

## Response

It's a 204 No Content Response.

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

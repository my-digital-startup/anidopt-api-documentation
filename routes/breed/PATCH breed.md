# API Route

## Name

`PATCH breed`

## Description

This route allows to modify a breed.

## Request

```
PATCH /api/v1/breeds/{id}
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`id: String`

## Body JSON

```JSON
{
  "name": "BREED name",
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Breed",
  "@id": "\/api\/v1\/breeds\/2",
  "@type": "Breed",
  "id": 2,
  "name": "BREED name",
  "createdAt": "2021-07-09T11:55:58+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`GET breeds`

## Description

This route retrieves all breeds.

## Request

```
GET /api/v1/breeds
```

## Query parameters

`None`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Breed",
  "@id": "\/api\/v1\/breeds",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/v1\/breeds\/1",
      "@type": "Breed",
      "id": 1,
      "name": "Dog breed 0vero",
      "createdAt": "2021-07-05T12:23:24+00:00"
    },
    {
      // An other breed
    },
  ],
  "hydra:totalItems": 30
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

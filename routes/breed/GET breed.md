# API Route

## Name

`GET breed`

## Description

This route retrieves a specific breed.

## Request

```
GET /api/v1/breeds/{id}
```

## Query parameters

`id: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Breed",
  "@id": "\/api\/v1\/breeds\/1",
  "@type": "Breed",
  "id": 1,
  "name": "Dog breed 0vero",
  "createdAt": "2021-07-05T12:23:24+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`PATCH tag`

## Description

This route allows to modify a tag.

## Request

```
PATCH /api/v1/tags/{id}
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`id: String`

## Body JSON

```JSON
{
  "name": "PATCH TAG name",
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Tag",
  "@id": "\/api\/v1\/tags\/1",
  "@type": "Tag",
  "id": 1,
  "name": "PATCH TAG name",
  "createdAt": "2021-07-09T12:22:25+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

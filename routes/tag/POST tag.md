# API Route

## Name

`POST tag`

## Description

This route allows to modify a tag.

## Request

```
POST /api/v1/tags/
```

## Query parameters

`None`

## Body JSON

```JSON
{
  "name": "POST TAG name",
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Tag",
  "@id": "\/api\/v1\/tags\/1",
  "@type": "Tag",
  "id": 1,
  "name": "POST TAG name",
  "createdAt": "2021-07-09T12:22:25+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

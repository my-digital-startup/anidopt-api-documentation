# API Route

## Name

`GET tags`

## Description

This route retrieves all tags.

## Request

```
GET /api/v1/tags
```

## Query parameters

`None`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Tag",
  "@id": "\/api\/v1\/tags",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/v1\/tags\/1",
      "@type": "Tag",
      "id": 1,
      "name": "Tag sapiente",
      "createdAt": "2021-07-05T12:23:24+00:00"
    },
    {
      // An other tag
    }
  ],
  "hydra:totalItems": 3
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

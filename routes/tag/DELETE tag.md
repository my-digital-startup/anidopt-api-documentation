# API Route

## Name

`DELETE tag`

## Description

This route delete a specific tag.

## Request

```
DELETE /api/v1/tags/{id}
```

## Query parameters

`id: String`

## Body JSON

`None`

## Response

It's a 204 No Content Response.

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

# API Route

## Name

`GET tag`

## Description

This route retrieves a specific tag.

## Request

```
GET /api/v1/tags/{id}
```

## Query parameters

`id: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Tag",
  "@id": "\/api\/v1\/tags\/1",
  "@type": "Tag",
  "id": 1,
  "name": "Tag sapiente",
  "createdAt": "2021-07-05T12:23:24+00:00"
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

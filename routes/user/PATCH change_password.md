# API Route

## Name

`PATCH change_password`

## Description

This route will manage the change of a user's password.

## Request

```
GET /api/v1/users/{uuid}/password
```

## Query parameters

`uuid: String`

## Body JSON

```JSON
{
  "oldPassword": "password",
  "newPassword": "password2"
}
```

## Response

```JSON
{
  "message": "Le mot de passe a bien été modifié."
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` <br/>
`ROLE_USER`

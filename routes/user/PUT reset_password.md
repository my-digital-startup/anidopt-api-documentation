# API Route

## Name

`PUT reset_ppasword`

## Description

This route will manage the reset of a user's password.

## Request

```
GET /api/v1/users/password/reset
```

## Query parameters

`None`

## Body JSON

### First step

```JSON
{
  "email": "adopter@anidopt.bzh",
}
```

### Second step

```JSON
{
  "email": "adopter@anidopt.bzh",
  "token": "0c2bca6e46f13fffc93c8295ff5b1251",
  "password": "password2"
}
```

## Response

### First step

```JSON
{
  "message": "L'email a bien été envoyé."
}
```

### Second step

```JSON
{
  "message": "Le mot de passe a bien été réinitialisé."
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

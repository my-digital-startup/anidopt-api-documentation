# API Route

## Name

`GET me`

## Description

This route retrieves the current user.

## Request

```
GET /api/v1/users/me
```

## Query parameters

`None`

## Body JSON

`None`

## Response

```JSON
{
  "id": null,
  "uuid": "2523b6a8-0bdd-4bd8-a6b4-c0192a555104",
  "firstName": "",
  "lastName": "Techer",
  "pseudo": "Rémy Breton",
  "email": "adopter@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_ADOPTER"
  ],
  "createdAt": "2021-06-12T16:51:24+00:00",
  "account": {
    "activationToken": "b916e8c61ce0e7cff788b54f4ed9d4ff",
    "isVerified": false,
    "resetPasswordToken": null,
    "phone": null,
    "address": "9, avenue Audrey Blot",
    "city": "Martineaunec",
    "zipCode": "27 467"
  }
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY`

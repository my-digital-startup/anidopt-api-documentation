# API Route

## Name

`GET user_media_objects`

## Description

This route retrieves all the media objects of a specific user.

## Request

```
GET /api/v1/users/{uuid}/media_objects
```

## Query parameters

`uuid: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/MediaObject",
  "@id": "\/api\/v1\/users\/1f82a78e-16a2-40f0-80f9-3a9464a4e9d7\/media_objects",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/v1\/media_objects\/1",
      "@type": "MediaObject",
      "contentUrl": "\/media\/60e83f816f6b3_bob.jpeg",
      "pet": "\/api\/v1\/pets\/16ec7495-7f7e-4831-90bc-0f1bd48b0318",
      "author": "\/api\/v1\/shelters\/1f82a78e-16a2-40f0-80f9-3a9464a4e9d7"
    },
    {
      // An other Media Object
    },
  ],
  "hydra:totalItems": 4
}
```

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

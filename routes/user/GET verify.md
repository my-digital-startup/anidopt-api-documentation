# API Route

## Name

`GET verify`

## Description

This route is used to verify a user.

## Request

```
GET /api/v1/users/verify
```

## Query parameters

`activationToken: String`
`expires: String`
`signature: String`
`token: String`

## Body JSON

`None`

## Response

Is a redirection.

## Authorization

`IS_AUTHENTICATED_ANONYMOUSLY`

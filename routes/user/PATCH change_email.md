# API Route

## Name

`PATCH change_email`

## Description

This route will manage the change of a user's email.

## Request

```
PATCH /api/v1/users/{uuid}/email
```

⚠️ You need to put the Content-Type like this :

`Content-Type: application/merge-patch+json`

instead of:

`Content-Type: application/json`

## Query parameters

`uuid: String`

## Body JSON

```JSON
{
  "email": "adopter@anidopt.bzh"
}
```

## Response

```JSON
{
  "@context": "\/api\/v1\/contexts\/Adopter",
  "@id": "\/api\/v1\/adopters\/4faa0348-2207-41f3-b18b-2fbe759dd784",
  "@type": "Adopter",
  "uuid": "4faa0348-2207-41f3-b18b-2fbe759dd784",
  "firstName": "Jacqueline",
  "lastName": "",
  "pseudo": "",
  "email": "adopterr@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_ADOPTER"
  ],
  "createdAt": "2021-06-16T07:33:45+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:879",
    "isVerified": true,
    "address": "97, boulevard Laroche",
    "city": "CollinVille",
    "zipCode": "26615"
  }
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` (Only the current user) <br/>
`ROLE_USER`

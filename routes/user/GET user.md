# API Route

## Name

`GET user`

## Description

This route retrieves a specific user.

## Request

```
GET /api/v1/users/{uuid}
```

## Query parameters

`uuid: String`

## Body JSON

`None`

## Response

```JSON
{
  "@context": "/api/v1/contexts/Adopter",
  "@id": "/api/v1/adopters/da6cae8d-ba85-4a8d-b32a-866f5600d348",
  "@type": "Adopter",
  "id": null,
  "uuid": "da6cae8d-ba85-4a8d-b32a-866f5600d348",
  "firstName": "Gabriel",
  "lastName": "Marechal",
  "pseudo": "",
  "email": "adopter@anidopt.bzh",
  "roles": [
    "ROLE_USER",
    "ROLE_ADOPTER"
  ],
  "createdAt": "2021-06-10T13:00:07+00:00",
  "account": {
    "@type": "Account",
    "@id": "_:829",
    "activationToken": "0f98af880486c40d80f36d9537489643",
    "isVerified": false,
    "resetPasswordToken": null
  }
}
```

## Authorization

`IS_AUTHENTICATED_SUCCESSFULLY` (Only the current user)

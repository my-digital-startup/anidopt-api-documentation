<!-- NAME -->

## Name

Anidopt API documentation

<!-- DESCRIPTION -->

## Description

Documentation about the Anidopt API

<!-- FEATURES -->

## Features

The features of the API:

- Authentication
- Change data
- Change email
- Change password
- Link media objects to a pet
- Registration
- Reset password

<!-- ROUTES -->

## Routes

### Adopter:

- GET /api/v1/adopter/{uuid}
- GET /api/v1/adopters
- PATCH /api/v1/adopters/{uuid}
- POST /api/v1/adopters
- GET /api/v1/adopters/{uuid}/pets

### Breed:

- DELETE /api/v1/breeds/{id}
- GET /api/v1/breeds/{id}
- GET /api/v1/breeds
- PATCH /api/v1/breeds/{id}
- POST /api/v1/breeds

### Media Object:

- DELETE /api/v1/media_objects/{id}
- GET /api/v1/media_objects/{id}
- GET /api/v1/media_objects
- POST /api/v1/media_objects

### Pet:

- DELETE /api/v1/pets/{uuid}
- GET /api/v1/pets/{uuid}
- GET /api/v1/pets
- PATCH /api/v1/pets/{uuid}
- POST /api/v1/pets

### Shelter:

- GET /api/v1/shelters/{uuid}
- GET /api/v1/shelters
- PATCH /api/v1/shelters/{uuid}
- POST /api/v1/shelters

### Specie:

- GET /api/v1/species/{id}
- GET /api/v1/species
- PATCH /api/v1/species/{id}
- POST /api/v1/species

### Tag:

- DELETE /api/v1/tags/{id}
- GET /api/v1/tags/{id}
- GET /api/v1/tags
- PATCH /api/v1/tags/{id}
- POST /api/v1/tags

### User

- GET /api/v1/users/me
- GET user_media_objects
- GET /api/v1/users/{uuid}
- GET /api/v1/users
- GET /api/v1/users/verify
- PATCH /api/v1/users/{uuid}/email
- PATCH /api/v1/users/{uuid}/password
- PUT /api/v1/users/password/reset

### Other

- GET /api/v1/token/refresh
